set dotenv-load
# https://github.com/casey/just

# Disable pagination of the output
export AWS_PAGER := ""
export AWS_PROFILE := "avcudenoir"
export PULUMI_CONFIG_PASSPHRASE := `sops -d pulumi.passphrase.encrypted | tr -d '\n'`
distribution_id := `steampipe query "select id from aws.aws_cloudfront_distribution where aliases -> 'Items' ? 'alanmatuszczak.me'" --output=csv --progress=false | tail -n 1`


default: jekyll aws-sync-files aws-invalidate-cdn

steampipe-preinstall:
  steampipe plugin install aws

jekyll:
  jekyll build --source ./jekyll

aws-sso-login:
  aws sso login

aws-copy-config:
  sops -d ./aws.config.encrypted > ${HOME}/.aws/config

aws-bootstrap: aws-copy-config aws-sso-login

aws-sync-files:
  aws s3 sync --delete ./_site s3://website-amatuszczak-599496227134-d8a7b02

aws-invalidate-cdn:
  aws cloudfront create-invalidation --distribution-id "{{distribution_id}}" --paths "/*"

[working-directory: 'jekyll']
bundle-update:
  bundle update --all

[working-directory: 'infra']
pulumi-diff:
  pulumi preview --diff

[working-directory: 'infra']
pulumi-up:
  pulumi up --diff
