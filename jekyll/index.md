---
layout: default
title: "Alan Matuszczak's Homepage"
---
## About me

{% include image.html url="/assets/img/avatar.webp" description="Hey, it's me!" %}

Strives for keeping things neat and automated.
Tries to avoid reinventing the wheel or falling to the "Not Invented Here" mindset.
Loves fast iteration, collaboration and no-bullshit attitude.
Fan of simple solutions. 

Currently happy with the position I'm in :).

### Interests
&nbsp;

* cycling,
* books (mainly Fantasy and S.F., occasionally Mystery),
* taking cat pictures (https://www.instagram.com/czarli_and_yenna/),
* video games (PS4, Xbox Series X and Nintendo Switch),
* headphones and watches (can't have too many, eh?),
* playing Dungeons and Dragons (both as a DM and a regular player),
* getting better at what I do :)
