---
layout: default
title: Experience
---
## Skills
&nbsp;

* Cloud: Amazon Web Services / Google Cloud Platform
* OS: Linux
* Container Orchestration: Kubernetes / Amazon Elastic Container Service
* Programming languages:  day-to-day -> [Python, Go], learning -> [Elixir], beginner -> [Ruby]
* IaaC: Terraform/Terragrunt, CDK (intermediate)
* Configuration Management: Ansible / Puppet (a bit rusty nowadays...)
* CI/CD: GitLab CI / CircleCI / Jenkins
* APM/Monitoring: DataDog / NewRelic / SignalFx / Prometheus / Grafana
* Logging: SumoLogic / Elasticsearch + Fluent[D/bit] / Filebeat

## Experience

### Staff DevOps Engineer II at _Housecall Pro_
##### April 2024 -- Present day | Poznań, Poland (Remotely)
&nbsp;

to be continued :)

### Staff DevOps Engineer I at _Housecall Pro_
##### June 2022 -- March 2024 | Poznań, Poland (Remotely)
&nbsp;

* Day-to-day organizational work: RFCs, feedbacks, technical QoL improvements, cross-{team, division} support, incident response
* Collaborated on reducing our Kubernetes toil and technical debt
* Overhauled our VPN setup, changed vendors and opted for a decentralized network segment access architecture
* Delivered key technical implementation and evaluation of PoC ETL solutions
* Involved in bootstrapping an eventing system for inter- and cross-application communication (Apache Kafka, Debezium)
* Led a bottoms-up initiative of migrating the main Rails monolith from MySQL 5.6 to MySQL 5.7 (on Amazon Aurora)

### Senior DevOps Engineer at _Housecall Pro_
##### October 2021 -- May 2022 | Poznań, Poland (Remotely)
&nbsp;

* Helped prototype, replace and invalidate old methods of authenticating employees into our AWS accounts with AWS SSO + Okta via SCIM
* Streamlined and centralized our cross-account AWS network traffic using AWS Transit Gateway
* Secured access to internal resources by implementing a split-traffic VPN-like solution
* Planned and migrated one of the major projects from MySQL to PostgreSQL (using AWS DMS)
* Involved in making our cloud infrastructure more compliant and auditable
* Introduced automatic rotation of database credentials (AWS Secrets Manager + custom Lambda functions)

### Senior DevOps Engineer at _Nethone_
##### March 2021 -- August 2021 | Poznań, Poland (Remotely)
&nbsp;

* Automated maintenance toil of patching Kubernetes workers (blue-green workload switchover), which was:
  * written in Python
  * aware of EC2 Autoscaling Groups and the connected set of workers to the API Server 
  * cordoning worker nodes using the Pod Eviction API
* Evaluated alternative CNI plugins (Calico, Cilium) in terms of performance benchmarks and feature capability requirements
* Fine-tuned the logging infrastructure (fluentd: simplified filter routing, better log-stream processing throughput)
* Provisioned, implemented observability and prepared a thorough documentation of a self-hosted application monitoring solution, used by all technical teams (Sentry)
  * stack composed of Clickhouse (K8s StatefulSet), Kafka (AWS MSK), Postgres (AWS RDS), Redis (AWS ElastiCache)
* Maintained and reduced the amount of tech debt in the legacy parts of the infrastructure, managed through Ansible

### Senior DevOps Engineer at _Netguru_
##### March 2020 -- February 2021 | Poznań, Poland (Remotely)
&nbsp;

* Improving the internal PaaS-like Terraform wrapper, written in Go
  * adding new features (cost estimation mechanism, more thorough infrastructure cost reduction automation)
  * increased the observability by setting up CloudWatch Logs and adding Prometheus instrumentation to critical business logic
  * debugged and fixed application's corner cases
  * expanded the technical and end-user documentations
* Mentoring and consultations
  * the go-to contact for a new Junior DevOps hire
  * explained infrastructure concepts (around AWS, containers and CI/CD) to developers, PMs and business clients,
  * provided help during a dedicated weekly calendar consultation slot
  * transferred knowledge to projects teams in order to make them more independent infrastructure-wise
* Organized a 3h-long internal workshop on Terraform, aimed at developers, during Netguru's [Burning Minds 2020 conference](https://www.netguru.com/burning-minds-2020)
* Delivered a set of opinionated infrastructure modules for common infrastructure/architecture patterns
* Solved a wide range of customer challenges in project assignments
  * Moving a production environment from ECS to EKS on Fargate
  * Improving CI pipelines
  * Preparing a ICMP-based solution for monitoring availability of six thousand network routers within a private network
  * Reducing cloud costs
  * Writing tooling that moves metrics between monitoring solutions

---

### Site Reliability Engineer at _OLX Group_
##### November 2019 -- February 2020 | Poznań, Poland
&nbsp;

* Organized an internal workshop on EKS (for fellow SREs)
* Documented/shared the architectural and infrastructural details of the few products I was involved in
* Prepared the new Application Management System-like product for release in terms of monitoring and reliability
* Adjusted the configuration of the Europe-wide CDN (Akamai)
* Committed to the efforts of launching rebranded Desktop and Mobile frontend experiences

---

### Junior Site Reliability Engineer at _OLX Group_
##### February 2019 -- October 2019 | Poznań, Poland
&nbsp;

* Took part in on-call rotations
* Performed online schema migrations on milliard-row Aurora MySQL tables using [gh-ost](https://github.com/github/gh-ost)
* Handled the configuration, documentation, deployment and monitoring of new staging/production RabbitMQ clusters
* Replaced certain shell-based maintenance tasks with AWS System Manager Commands
* Participated in an internal AWS EKS Workshop aiming to allow multiple teams use Kubernetes easily
* Managed AWS Resources with Terraform
* Coordinated turning on of New Relic's Distributed Tracing for 6 countries (and infrastructure stacks) OLX "Europe" operates in
* Migrated staging and production deployment processes from CodePipeline to GitLab CI to unify all CI stages in a single pipeline; feature parity was achieved using an in-house Python script
* Prepared AWS ElasticBeanstalk environments, Aurora MySQL clusters, CI pipelines and isolated VPCs for a CMS-like portal for the 'Jobs' classifieds category
* Contributed to EU Shared EKS cluster internal components configuration: logging (fluentd), DNS (external-dns with Route53), RBAC (custom Helm chart to manage user access)VPCLink-available internal LB (based on nginx-ingress) and monitoring (both Prometheus and SignalFx)
* Debugged, fixed and maintained EKS environments for three different packs/teams
* Provided input to the Jobs team's backend architecture and have set up auxiliary services (AWS API Gateway and DocumentDB/MongoDB) for it

---

### DevOps Engineer at _Flyps_
##### September 2018 -- February 2019 | Poznań, Poland
&nbsp;

Main goals:

* reducing the developers worries by moving the application stack to a stable cloud-based infrastructure
* pioneering a version-controlled stack configuration on Kubernetes

Detailed actions:

* Migrated a Rancher-based, on-premise production infrastructure to Google Kubernetes Engine
* Wrote a series of Helm charts managing the microservice stack
* Simplified the deployment of Helm charts using Helmfile
* Assessed Azure Kubernetes Service as a production deployment target
* Introduced Prometheus as the central monitoring solution
* Offloaded basic uptime checks to Stackdriver Monitoring
* Managed an Elastic Cloud instance and pushed logs to it using Filebeat and Logstash
* Performed an evaluation deployment of Apache Kafka using the Strimzi operator + benchmarking
* Wrote Prometheus rules and alerts
* Perfomed Kubernetes resource-based management of DNS (external-dns) and SSL (cert-manager)
* Made secrets encrypted using Google Cloud KMS and Mozilla's sops
* Reduced maintenance overhead by migrating CI jobs from Jenkins to Gitlab CI
* Used Terraform to:

  * manage static DNS entries (Cloudflare)
  * setup networking, firewalls, storage, IAM and Kubernetes clusters in the cloud (Google Cloud Platform and Microsoft Azure)
  * simplify management of multiple GCP project and Kubernetes clusters by grouping resources in Terraform modules
  * deploy highly available MySQL (CloudSQL) instancesorts of launching rebranded Desktop and Mobile frontend experiences

---

### DevOps Engineer at _Egnyte_
##### March 2018 -- August 2018 | Poznań, Poland
&nbsp;

* Prepared the infrastructure and CI/CD process for a new video transcoding service, deployed on GKE
* Brought immutable VMs into existing legacy CI pipelines

---

### Junior Build Release Engineer at _Egnyte_
##### July 2016 -- February 2018 | Poznań, Poland
&nbsp;

Practical experience with:

* setup, deployment and maintenance of:

  * internet-facing applications (Sonatype Nexus, Jenkins, AWX)
  * packaging (with additional signing and hosting of repositories)
  * on-premise VM appliances (Alpine/CentOS/Debian-based)
  * Cloud-managed (GKE) Kubernetes clusters
  * CI servers (Jenkins, Gitlab) and nodes (QA and production environments)
* automatization of OS install and setup processes (Vagrant, Packer)
* OS scripting
* Configuration management tools:

  * Ansible - automation of regular maintenance tasks; helper roles for CI integration of OSX and Windows VMs
  * Puppet - managing CI Linux boxes, improvement of existing modules
* Hypervisors/Virtualization (ESXi, Virtualbox)
* Infrastructure as Code (Terraform)
* server applications debugging
* monitoring (Kibana, Nagios and New Relic)
* containerization of services
* writing and improving Continuous Integration/Delivery pipelines
* configuring web servers (nginx)
* networking (basic)
* managing CI jobs in the following ecosystems and languages:

  * Android
  * iOS/OSX
  * C++ (Windows)
  * C#
  * Python
  * Go
  * Ruby

---

### Build Release Engineer Intern at _Egnyte_
##### February 2016 -- July 2016 | Poznań, Poland
&nbsp;

* Automated Jenkins agents configuration using Puppet and Ansible.
* Wrote and improved Continuous Integration/Delivery pipelines
* Increased the stability and performance of production Jenkins server by setting up backups, tweaking the JVM and automating the process of setting up new instances for testing purposes
