---
layout: default
title: Education
---
## Education

**M.Sc. Eng.** in **Computer Science**:


* **Institution**: Adam Mickiewicz University, Poznań, Poland

* **Thesis**:

  * **Subject**: Comparison of selected infrastructure management automation tools

  * **Faculty**: Department 	Faculty of Mathematics and Computer Science

* **Year of graduation**: 2016
