import pulumi
import pulumi_aws as aws
from pulumi_aws_tags import register_auto_tags

register_auto_tags(
    {
        "pulumi:Owned": "yes",
        "pulumi:Project": pulumi.get_project(),
        "pulumi:Stack": pulumi.get_stack(),
    }
)

current = aws.get_caller_identity()

bucket = aws.s3.BucketV2(f"website-amatuszczak-{current.account_id}")

aws.s3.BucketServerSideEncryptionConfigurationV2(
    "ss3",
    bucket=bucket.id,
    rules=[{"apply_server_side_encryption_by_default": {"sse_algorithm": "AES256"}}],
)


oac = aws.cloudfront.OriginAccessControl(
    "oac_s3",
    name="website_oac",
    origin_access_control_origin_type="s3",
    signing_behavior="always",
    signing_protocol="sigv4",
)

s3_origin_id = "pulumis3website"

website_domains = [f"{x}alanmatuszczak.me" for x in ["", "www."]]

cert = aws.acm.Certificate(
    "acm_website_cert",
    domain_name=website_domains[1],
    key_algorithm="EC_prime256v1",
    subject_alternative_names=website_domains,
    validation_method="DNS",
)

website = aws.cloudfront.Distribution(
    "website",
    origins=[
        {
            "domain_name": bucket.bucket_domain_name,
            "origin_id": s3_origin_id,
            "origin_access_control_id": oac.id,
        }
    ],
    enabled=True,
    is_ipv6_enabled=True,
    aliases=website_domains,
    http_version="http2and3",
    price_class="PriceClass_100",
    comment="Main website Distribution, managed by Pulumi",
    default_root_object="index.html",
    default_cache_behavior={
        "cache_policy_id": "658327ea-f89d-4fab-a63d-7e88639e58f6",
        "allowed_methods": [
            "GET",
            "HEAD",
            "OPTIONS",
        ],
        "cached_methods": [
            "GET",
            "HEAD",
            "OPTIONS",
        ],
        "viewer_protocol_policy": "redirect-to-https",
        "target_origin_id": s3_origin_id,
        "compress": True,
    },
    restrictions={
        "geo_restriction": {
            "restriction_type": "blacklist",
            "locations": [
                "CN",
                "RU",
            ],
        },
    },
    viewer_certificate={
        "minimum_protocol_version": "TLSv1.2_2021",
        "acm_certificate_arn": cert.arn,
        "ssl_support_method": "sni-only",
    },
    custom_error_responses=[
        {
            "error_code": 404,
            "error_caching_min_ttl": 60,
            "response_code": 200,
            "response_page_path": "/index.html",
        }
    ],
)

s3_oac_policy = aws.iam.get_policy_document_output(
    statements=[
        {
            "principals": [
                {
                    "type": "Service",
                    "identifiers": ["cloudfront.amazonaws.com"],
                }
            ],
            "actions": [
                "s3:GetObject",
            ],
            "resources": [bucket.arn.apply(lambda v: f"{v}/*")],
            "conditions": [
                {
                    "test": "StringEquals",
                    "variable": "AWS:SourceArn",
                    "values": [
                        website.arn,
                    ],
                }
            ],
        }
    ]
)

aws.s3.BucketPolicy("CF_OAC_ACCESS", bucket=bucket.id, policy=s3_oac_policy.json)

# Export the name of the bucket
pulumi.export("bucket_name", bucket.id)
